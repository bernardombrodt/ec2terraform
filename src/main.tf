terraform {
  required_version = ">=0.13"
}
provider "aws" {
  region = "us-east-2"

}

#instancias automaticas 3 unidades sem bucket
resource "aws_instance" "dev" {
  count         = 3
  ami           = "ami-07efac79022b86107"
  instance_type = "t2.micro"
  key_name      = "ec2terraform"
  tags = {
    Name = "dev${count.index}"
  }
  vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}"]
}
#Grupo de segurança
resource "aws_security_group" "allow_ssh" {
  name        = "ssh"
  description = "Allow TLS inbound traffic"
  vpc_id      = "vpc-07c0636c"


  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["189.6.238.120/32"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_instance" "dev5" {
  ami           = "ami-07efac79022b86107"
  instance_type = "t2.micro"
  key_name      = "ec2terraform"
  tags = {
    Name = "dev5"
  }
  vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}"]
}